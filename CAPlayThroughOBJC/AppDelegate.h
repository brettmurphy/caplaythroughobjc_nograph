//
//  AppDelegate.h
//  CAPlayThroughOBJC
//
//  Created by Brett Murphy on 13/03/13.
//  Copyright (c) 2013 CleverWEB. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>
@property (assign) IBOutlet NSWindow *window;

@end


#include "CAPlayThrough.h"

#pragma mark -- CAPlayThrough

// we define the class here so that is is not accessible from any object aside from CAPlayThroughManager
class CAPlayThrough 
{
public:
	CAPlayThrough(AudioDeviceID input, AudioDeviceID output);
	~CAPlayThrough();
	
	OSStatus	Init(AudioDeviceID input, AudioDeviceID output);
	void		Cleanup();
	OSStatus	Start();
	OSStatus	Stop();
	Boolean		IsRunning();
	OSStatus	SetInputDeviceAsCurrent(AudioDeviceID in);
	OSStatus	SetOutputDeviceAsCurrent(AudioDeviceID out);
	
	AudioDeviceID GetInputDeviceID()	{ return mInputDevice.mID;	}
	AudioDeviceID GetOutputDeviceID()	{ return mOutputDevice.mID; }
	

private:
	OSStatus SetupOutput(AudioDeviceID out);
	OSStatus SetupAUHAL(AudioDeviceID in);
	OSStatus SetupBuffers();	
	void ComputeThruOffset();	
	static OSStatus InputProc(void *inRefCon, AudioUnitRenderActionFlags *ioActionFlags,const AudioTimeStamp *inTimeStamp,UInt32 inBusNumber,UInt32 inNumberFrames,AudioBufferList *ioData);	
	static OSStatus OutputProc(void *inRefCon,AudioUnitRenderActionFlags *ioActionFlags,const AudioTimeStamp *inTimeStamp,UInt32 inBusNumber,UInt32 inNumberFrames,AudioBufferList *ioData);
    OSStatus CreateAUFromDescription(AudioUnit *newAU,OSType componentType,OSType componentSubType,OSType componentManufacturer);
    OSStatus ConnectAUs(AudioUnit fromAU,AudioUnit toAU,UInt32 sourceOutputNumber,UInt32 destInputNumber,AudioUnitScope scope,AudioUnitElement inElement);
    
	AudioUnit mInputUnit;
	AudioBufferList *mInputBuffer;
	AudioDevice mInputDevice, mOutputDevice;
	CARingBuffer *mBuffer;
	
	//AudioUnits and Graph
	//AUGraph mGraph;
	AUNode mVarispeedNode;
	AudioUnit mVarispeedUnit;
	AUNode mOutputNode;
	AudioUnit mOutputUnit;
	
	//Buffer sample info
	Float64 mFirstInputTime;
	Float64 mFirstOutputTime;
	Float64 mInToOutSampleOffset;
};

#pragma mark ---Public Methods---

#pragma mark ---CAPlayThrough Methods---
CAPlayThrough::CAPlayThrough(AudioDeviceID input, AudioDeviceID output): mBuffer(NULL), mFirstInputTime(-1), mFirstOutputTime(-1), mInToOutSampleOffset(0) {
	OSStatus err = noErr;
	err = Init(input,output);
    if(err) {
		fprintf(stderr,"CAPlayThrough ERROR: Cannot Init CAPlayThrough");
		exit(1);
	}
}

CAPlayThrough::~CAPlayThrough() {   
	Cleanup();
}

//BM convenience method to connect to AU's together
OSStatus CAPlayThrough::ConnectAUs(AudioUnit fromAU,AudioUnit toAU,UInt32 sourceOutputNumber,UInt32 destInputNumber,AudioUnitScope scope,AudioUnitElement inElement) {
    OSStatus err = noErr;
    AudioUnitConnection auconnection;
    auconnection.sourceAudioUnit = fromAU;
    auconnection.sourceOutputNumber = sourceOutputNumber;
    auconnection.destInputNumber = destInputNumber;
    err = AudioUnitSetProperty(toAU, kAudioUnitProperty_MakeConnection, scope, inElement,  (void *)&auconnection, sizeof(AudioUnitConnection));
    return err;
}

OSStatus CAPlayThrough::Init(AudioDeviceID input, AudioDeviceID output) {
    OSStatus err = noErr;
	//Setup AUHAL for an input device
	err = SetupAUHAL(input); checkErr(err);
	//Setup Graph containing Varispeed Unit & Default Output Unit
	err = SetupOutput(output); checkErr(err);
	err = SetupBuffers(); checkErr(err);
    //BM added now connect varispeed and output since we have no graph anymore
    err = ConnectAUs(mVarispeedUnit,mOutputUnit,0,0,kAudioUnitScope_Input,0);checkErr(err);    
    //init the units
    err = AudioUnitInitialize(mVarispeedUnit); checkErr(err);
    err = AudioUnitInitialize(mOutputUnit); checkErr(err);	
	//Add latency between the two devices
	ComputeThruOffset();		
	return err;	
}

void CAPlayThrough::Cleanup() {
	//clean up
	Stop();									
	delete mBuffer;
	mBuffer = 0;
	if(mInputBuffer){
		for(UInt32 i = 0; i<mInputBuffer->mNumberBuffers; i++)
			free(mInputBuffer->mBuffers[i].mData);
		free(mInputBuffer);
		mInputBuffer = 0;
	}
	
	AudioUnitUninitialize(mInputUnit);
    AudioUnitUninitialize(mOutputUnit);
    AudioUnitUninitialize(mVarispeedUnit);

    AudioComponentInstanceDispose(mInputUnit);
    AudioComponentInstanceDispose(mOutputUnit);
    AudioComponentInstanceDispose(mVarispeedUnit);
}

#pragma mark --- Operation---

OSStatus CAPlayThrough::Start() {
	OSStatus err = noErr;
	if(!IsRunning()){		
		//Start pulling for audio data
		err = AudioOutputUnitStart(mInputUnit); checkErr(err);
        
        //start the units
        err = AudioOutputUnitStart(mOutputUnit); checkErr(err);

		//reset sample times
		mFirstInputTime = -1;
		mFirstOutputTime = -1;
	}
	return err;	
}

OSStatus CAPlayThrough::Stop() {
	OSStatus err = noErr;
	if(IsRunning()) {
		//Stop the AUHAL
		err = AudioOutputUnitStop(mInputUnit); checkErr(err);
        err = AudioOutputUnitStop(mOutputUnit); checkErr(err);

		mFirstInputTime = -1;
		mFirstOutputTime = -1;
	}
	return err;
}

Boolean CAPlayThrough::IsRunning() {	
	OSStatus err = noErr;
	UInt32 mInputUnitIsRunning = 0, size = 0;
    UInt32 mOutputUnitIsRunning = 0;
	size = sizeof(mInputUnitIsRunning);
	if(mInputUnit)  {
		err = AudioUnitGetProperty(mInputUnit,kAudioOutputUnitProperty_IsRunning,kAudioUnitScope_Global,0,&mInputUnitIsRunning,&size);checkErr(err);
    }
    size = sizeof(mOutputUnitIsRunning);
    if(mOutputUnit) {
		err = AudioUnitGetProperty(mOutputUnit,kAudioOutputUnitProperty_IsRunning,kAudioUnitScope_Global,0,&mOutputUnitIsRunning,&size);checkErr(err);
    }

	return (mInputUnitIsRunning || mOutputUnitIsRunning);
}


OSStatus CAPlayThrough::SetOutputDeviceAsCurrent(AudioDeviceID out) {
    UInt32 size = sizeof(AudioDeviceID);;
    OSStatus err = noErr;
 
    AudioObjectPropertyAddress theAddress = { kAudioHardwarePropertyDefaultOutputDevice, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster };
	
	if(out == kAudioDeviceUnknown) //Retrieve the default output device
	{
		err = AudioObjectGetPropertyData(kAudioObjectSystemObject, &theAddress, 0, NULL, &size, &out); checkErr(err);
	}
	mOutputDevice.Init(out, false);
	
	//Set the Current Device to the Default Output Unit.
    err = AudioUnitSetProperty(mOutputUnit,kAudioOutputUnitProperty_CurrentDevice, kAudioUnitScope_Global, 0, &mOutputDevice.mID, sizeof(mOutputDevice.mID));
							
	return err;
}

OSStatus CAPlayThrough::SetInputDeviceAsCurrent(AudioDeviceID in) {
    UInt32 size = sizeof(AudioDeviceID);
    OSStatus err = noErr;
    
    AudioObjectPropertyAddress theAddress = { kAudioHardwarePropertyDefaultInputDevice, kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster };
	
	if(in == kAudioDeviceUnknown) //get the default input device if device is unknown
	{
		err = AudioObjectGetPropertyData(kAudioObjectSystemObject, &theAddress, 0, NULL, &size, &in); checkErr(err);
	}
	mInputDevice.Init(in, true);
	
	//Set the Current Device to the AUHAL.
	//this should be done only after IO has been enabled on the AUHAL.
    err = AudioUnitSetProperty(mInputUnit,kAudioOutputUnitProperty_CurrentDevice,kAudioUnitScope_Global, 0, &mInputDevice.mID,sizeof(mInputDevice.mID)); checkErr(err);
	return err;
}

#pragma mark -
#pragma mark --Private methods---

//convenience method to create an audio unit from the three description fields
OSStatus CAPlayThrough::CreateAUFromDescription(AudioUnit *newAU,OSType componentType,OSType componentSubType,OSType componentManufacturer) {
    OSStatus err = noErr;
    AudioComponent comp;
    AudioComponentDescription desc;
	desc.componentType = componentType;
	desc.componentSubType = componentSubType;
	desc.componentManufacturer = componentManufacturer;
	desc.componentFlags = 0;
	desc.componentFlagsMask = 0;	
    comp = AudioComponentFindNext(NULL, &desc);
	if (comp == NULL){
		printf ("can't get audio unit for %d,%d,%d\n",componentType,componentSubType,componentManufacturer); exit (-1); //change these  from uints to four char codes later
    }
    err = AudioComponentInstanceNew(comp, newAU); 
    return err;
}
 
OSStatus CAPlayThrough::SetupOutput(AudioDeviceID out) {
	OSStatus err = noErr;
	AURenderCallbackStruct output;
    err = CreateAUFromDescription(&mVarispeedUnit,kAudioUnitType_FormatConverter,kAudioUnitSubType_Varispeed,kAudioUnitManufacturer_Apple); checkErr(err);
    err = CreateAUFromDescription(&mOutputUnit,kAudioUnitType_Output,kAudioUnitSubType_DefaultOutput,kAudioUnitManufacturer_Apple); checkErr(err);
   	err = SetOutputDeviceAsCurrent(out); checkErr(err);
	
	//Tell the output unit not to reset timestamps 
	//Otherwise sample rate changes will cause sync los
	UInt32 startAtZero = 0;
	err = AudioUnitSetProperty(mOutputUnit, kAudioOutputUnitProperty_StartTimestampsAtZero, kAudioUnitScope_Global,  0, &startAtZero, sizeof(startAtZero)); checkErr(err);
	output.inputProc = OutputProc;
	output.inputProcRefCon = this;
	err = AudioUnitSetProperty(mVarispeedUnit, kAudioUnitProperty_SetRenderCallback,  kAudioUnitScope_Input, 0, &output, sizeof(output)); checkErr(err);
	return err;
} 

OSStatus CAPlayThrough::SetupAUHAL(AudioDeviceID in) {
	OSStatus err = noErr;
    err = CreateAUFromDescription(&mInputUnit,kAudioUnitType_Output,kAudioUnitSubType_HALOutput,kAudioUnitManufacturer_Apple); checkErr(err);    
	//AUHAL needs to be initialized before anything is done to it
	err = AudioUnitInitialize(mInputUnit); checkErr(err);	
	UInt32 enableIO =1;
	err =  AudioUnitSetProperty(mInputUnit,kAudioOutputUnitProperty_EnableIO,kAudioUnitScope_Input,1,&enableIO,sizeof(enableIO)); checkErr(err);
	enableIO = 0;
	err = AudioUnitSetProperty(mInputUnit,kAudioOutputUnitProperty_EnableIO,kAudioUnitScope_Output,0,&enableIO,sizeof(enableIO));
	err= SetInputDeviceAsCurrent(in); checkErr(err);	
    AURenderCallbackStruct input;
    input.inputProc = InputProc;
    input.inputProcRefCon = this;
	//Setup the input callback.
	err = AudioUnitSetProperty(mInputUnit,kAudioOutputUnitProperty_SetInputCallback,kAudioUnitScope_Global, 0,&input,sizeof(input)); checkErr(err);
	err = AudioUnitInitialize(mInputUnit);
	return err;
}

//Allocate Audio Buffer List(s) to hold the data from input.
OSStatus CAPlayThrough::SetupBuffers() {
	OSStatus err = noErr;
	UInt32 bufferSizeFrames,bufferSizeBytes,propsize;	
	CAStreamBasicDescription asbd,asbd_dev1_in,asbd_dev2_out;			
	Float64 rate=0;
	UInt32 propertySize = sizeof(bufferSizeFrames);
	err = AudioUnitGetProperty(mInputUnit, kAudioDevicePropertyBufferFrameSize, kAudioUnitScope_Global, 0, &bufferSizeFrames, &propertySize); checkErr(err);
    bufferSizeBytes = bufferSizeFrames * sizeof(Float32);
	propertySize = sizeof(asbd_dev1_in);
	err = AudioUnitGetProperty(mInputUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 1, &asbd_dev1_in, &propertySize); checkErr(err);
	propertySize = sizeof(asbd);
	err = AudioUnitGetProperty(mInputUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 1, &asbd, &propertySize); checkErr(err);
	propertySize = sizeof(asbd_dev2_out);
	err = AudioUnitGetProperty(mOutputUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 0, &asbd_dev2_out, &propertySize); checkErr(err);
	asbd.mChannelsPerFrame =((asbd_dev1_in.mChannelsPerFrame < asbd_dev2_out.mChannelsPerFrame) ?asbd_dev1_in.mChannelsPerFrame :asbd_dev2_out.mChannelsPerFrame) ;
	propertySize = sizeof(Float64);
    AudioObjectPropertyAddress theAddress = { kAudioDevicePropertyNominalSampleRate,kAudioObjectPropertyScopeGlobal, kAudioObjectPropertyElementMaster };                                              
    err = AudioObjectGetPropertyData(mInputDevice.mID, &theAddress, 0, NULL, &propertySize, &rate); checkErr(err);
    asbd.mSampleRate =rate;
	propertySize = sizeof(asbd);
	err = AudioUnitSetProperty(mInputUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 1, &asbd, propertySize); checkErr(err);	
	err = AudioUnitSetProperty(mVarispeedUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &asbd, propertySize); checkErr(err);
	propertySize = sizeof(Float64);
    err = AudioObjectGetPropertyData(mOutputDevice.mID, &theAddress, 0, NULL, &propertySize, &rate); checkErr(err);
    asbd.mSampleRate =rate;
	propertySize = sizeof(asbd);
	err = AudioUnitSetProperty(mVarispeedUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 0, &asbd, propertySize); checkErr(err);	
	err = AudioUnitSetProperty(mOutputUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 0, &asbd, propertySize); checkErr(err);
	propsize = offsetof(AudioBufferList, mBuffers[0]) + (sizeof(AudioBuffer) *asbd.mChannelsPerFrame);
	mInputBuffer = (AudioBufferList *)malloc(propsize);
	mInputBuffer->mNumberBuffers = asbd.mChannelsPerFrame;
	for(UInt32 i =0; i< mInputBuffer->mNumberBuffers ; i++) {
		mInputBuffer->mBuffers[i].mNumberChannels = 1;
		mInputBuffer->mBuffers[i].mDataByteSize = bufferSizeBytes;
		mInputBuffer->mBuffers[i].mData = malloc(bufferSizeBytes);
	}
	mBuffer = new CARingBuffer();	
	mBuffer->Allocate(asbd.mChannelsPerFrame, asbd.mBytesPerFrame, bufferSizeFrames * 20);
	
    return err;
}

void CAPlayThrough::ComputeThruOffset() {
	mInToOutSampleOffset = SInt32(mInputDevice.mSafetyOffset +  mInputDevice.mBufferSizeFrames + mOutputDevice.mSafetyOffset + mOutputDevice.mBufferSizeFrames);
}

#pragma mark -- IO Procs --
OSStatus CAPlayThrough::InputProc(void *inRefCon,AudioUnitRenderActionFlags *ioActionFlags,const AudioTimeStamp *inTimeStamp,UInt32 inBusNumber,UInt32 inNumberFrames,AudioBufferList * ioData) {
    OSStatus err = noErr;	
	CAPlayThrough *This = (CAPlayThrough *)inRefCon;
	if (This->mFirstInputTime < 0.)
		This->mFirstInputTime = inTimeStamp->mSampleTime;
		
	//Get the new audio data
	err = AudioUnitRender(This->mInputUnit,ioActionFlags,inTimeStamp, inBusNumber, inNumberFrames,This->mInputBuffer);checkErr(err);
		
	if(!err) {
		err = This->mBuffer->Store(This->mInputBuffer, Float64(inNumberFrames), SInt64(inTimeStamp->mSampleTime));
	}	

	return err;
}

inline void MakeBufferSilent (AudioBufferList * ioData)
{
	for(UInt32 i=0; i<ioData->mNumberBuffers;i++)
		memset(ioData->mBuffers[i].mData, 0, ioData->mBuffers[i].mDataByteSize);	
}

OSStatus CAPlayThrough::OutputProc(void *inRefCon,AudioUnitRenderActionFlags *ioActionFlags,const AudioTimeStamp *TimeStamp,UInt32 inBusNumber,UInt32 inNumberFrames,AudioBufferList * ioData) {
    OSStatus err = noErr;
	CAPlayThrough *This = (CAPlayThrough *)inRefCon;
	Float64 rate = 0.0;
	AudioTimeStamp inTS, outTS;		
	if (This->mFirstInputTime < 0.) {
		MakeBufferSilent (ioData);
		return noErr;
	}
	err = AudioDeviceGetCurrentTime(This->mInputDevice.mID, &inTS);
	if (err) {
		MakeBufferSilent (ioData);
		return noErr;
	}
	err = AudioDeviceGetCurrentTime(This->mOutputDevice.mID, &outTS); checkErr(err);
	rate = inTS.mRateScalar / outTS.mRateScalar;
	err = AudioUnitSetParameter(This->mVarispeedUnit,kVarispeedParam_PlaybackRate,kAudioUnitScope_Global,0, rate,0); checkErr(err);
	if (This->mFirstOutputTime < 0.) {
		This->mFirstOutputTime = TimeStamp->mSampleTime;
		Float64 delta = (This->mFirstInputTime - This->mFirstOutputTime);
		This->ComputeThruOffset();   
		//changed: 3865519 11/10/04
		if (delta < 0.0)
			This->mInToOutSampleOffset -= delta;
		else
			This->mInToOutSampleOffset = -delta + This->mInToOutSampleOffset;
					
		MakeBufferSilent (ioData);
		return noErr;
	}

	//copy the data from the buffers	
	err = This->mBuffer->Fetch(ioData, inNumberFrames, SInt64(TimeStamp->mSampleTime - This->mInToOutSampleOffset));	
	if(err != kCARingBufferError_OK)
	{
		MakeBufferSilent (ioData);
		SInt64 bufferStartTime, bufferEndTime;
		This->mBuffer->GetTimeBounds(bufferStartTime, bufferEndTime);
		This->mInToOutSampleOffset = TimeStamp->mSampleTime - bufferStartTime;
	}

	return noErr;
}
									
#pragma mark -- CAPlayThroughHost Methods --

CAPlayThroughHost::CAPlayThroughHost(AudioDeviceID input, AudioDeviceID output):mPlayThrough(NULL) {
	CreatePlayThrough(input, output);
}

CAPlayThroughHost::~CAPlayThroughHost() {
	DeletePlayThrough();
}

void CAPlayThroughHost::CreatePlayThrough(AudioDeviceID input, AudioDeviceID output) {
	mPlayThrough = new CAPlayThrough(input, output);
    StreamListenerQueue = dispatch_queue_create("com.CAPlayThough.StreamListenerQueue", DISPATCH_QUEUE_SERIAL);
	AddDeviceListeners(input);
}

void CAPlayThroughHost::DeletePlayThrough() {
	if(mPlayThrough)
	{
		mPlayThrough->Stop();
		RemoveDeviceListeners(mPlayThrough->GetInputDeviceID());
        dispatch_release(StreamListenerQueue);
        StreamListenerQueue = NULL;
		delete mPlayThrough;
		mPlayThrough = NULL;
	}
}

void CAPlayThroughHost::ResetPlayThrough () {
	AudioDeviceID input = mPlayThrough->GetInputDeviceID();
	AudioDeviceID output = mPlayThrough->GetOutputDeviceID();

	DeletePlayThrough();
	CreatePlayThrough(input, output);
	mPlayThrough->Start();
}

bool CAPlayThroughHost::PlayThroughExists() {
	return (mPlayThrough != NULL) ? true : false;
}

OSStatus	CAPlayThroughHost::Start() {
	if (mPlayThrough) return mPlayThrough->Start();
	return noErr;
}

OSStatus	CAPlayThroughHost::Stop() {
	if (mPlayThrough) return mPlayThrough->Stop();
	return noErr;
}

Boolean		CAPlayThroughHost::IsRunning() {
	if (mPlayThrough) return mPlayThrough->IsRunning();
	return noErr;
}

void CAPlayThroughHost::AddDeviceListeners(AudioDeviceID input)
{
    // creating the block here allows us access to the this pointer so we can call Reset when required
    AudioObjectPropertyListenerBlock listenerBlock = ^(UInt32 inNumberAddresses, const AudioObjectPropertyAddress inAddresses[]) {
        ResetPlayThrough();
    };
    
    // need to retain the listener block so that we can remove it later
    StreamListenerBlock = Block_copy(listenerBlock);
    
    AudioObjectPropertyAddress theAddress = { kAudioDevicePropertyStreams, kAudioDevicePropertyScopeInput, kAudioObjectPropertyElementMaster };
    
    // StreamListenerBlock is called whenever the sample rate changes (as well as other format characteristics of the device)
	UInt32 propSize;
	OSStatus err = AudioObjectGetPropertyDataSize(input, &theAddress, 0, NULL, &propSize);
    if (err) fprintf(stderr, "Error %ld returned from AudioObjectGetPropertyDataSize\n", (long)err);
    
	if(!err) {
        AudioStreamID *streams = (AudioStreamID*)malloc(propSize);	
		err = AudioObjectGetPropertyData(input, &theAddress, 0, NULL, &propSize, streams);
        if (err) fprintf(stderr, "Error %ld returned from AudioObjectGetPropertyData\n", (long)err);
        		
		if(!err) {
			UInt32 numStreams = propSize / sizeof(AudioStreamID);
			
            for(UInt32 i=0; i < numStreams; i++) {
				UInt32 isInput;
				propSize = sizeof(UInt32);
                theAddress.mSelector = kAudioStreamPropertyDirection;
                theAddress.mScope = kAudioObjectPropertyScopeGlobal;
				
                err = AudioObjectGetPropertyData(streams[i], &theAddress, 0, NULL, &propSize, &isInput);
                if (err) fprintf(stderr, "Error %ld returned from AudioObjectGetPropertyData\n", (long)err);

                if(!err && isInput) {
                    theAddress.mSelector = kAudioStreamPropertyPhysicalFormat;
                    err = AudioObjectAddPropertyListenerBlock(streams[i], &theAddress, StreamListenerQueue, StreamListenerBlock);
                    if (err) fprintf(stderr, "Error %ld returned from AudioObjectAddPropertyListenerBlock\n", (long)err);	
                }
            }
        }
        
        if (NULL != streams) free(streams);
    }
}

void CAPlayThroughHost::RemoveDeviceListeners(AudioDeviceID input) {
    AudioObjectPropertyAddress theAddress = { kAudioDevicePropertyStreams, kAudioDevicePropertyScopeInput,  kAudioObjectPropertyElementMaster };
                                              
	UInt32 propSize;
    OSStatus err = AudioObjectGetPropertyDataSize(input, &theAddress, 0, NULL, &propSize);
    if (err) fprintf(stderr, "Error %ld returned from AudioObjectGetPropertyDataSize\n", (long)err);

	if(!err) {
    
		AudioStreamID *streams = (AudioStreamID*)malloc(propSize);
        err = AudioObjectGetPropertyData(input, &theAddress, 0, NULL, &propSize, streams);
        if (err) fprintf(stderr, "Error %ld returned from AudioObjectGetPropertyData\n", (long)err);
        		
        if(!err) {
			UInt32 numStreams = propSize / sizeof(AudioStreamID);
			
            for(UInt32 i=0; i < numStreams; i++) {
				UInt32 isInput;
				propSize = sizeof(UInt32);
                theAddress.mSelector = kAudioStreamPropertyDirection;
                theAddress.mScope = kAudioObjectPropertyScopeGlobal;
                
                err = AudioObjectGetPropertyData(streams[i], &theAddress, 0, NULL, &propSize, &isInput);
                if (err) fprintf(stderr, "Error %ld returned from AudioObjectGetPropertyData\n", (long)err);

				if(!err && isInput) {
                    theAddress.mSelector = kAudioStreamPropertyPhysicalFormat;
                    
                    err = AudioObjectRemovePropertyListenerBlock(streams[i], &theAddress, StreamListenerQueue, StreamListenerBlock);
                    if (err) fprintf(stderr, "Error %ld returned from AudioObjectRemovePropertyListenerBlock\n", (long)err);
                    Block_release(StreamListenerBlock);
                }
			}
		}
        
        if (NULL != streams) free(streams);
	}
}
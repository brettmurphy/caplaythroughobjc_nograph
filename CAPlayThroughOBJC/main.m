//
//  main.m
//  CAPlayThroughOBJC
//
//  Created by Brett Murphy on 13/03/13.
//  Copyright (c) 2013 CleverWEB. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
